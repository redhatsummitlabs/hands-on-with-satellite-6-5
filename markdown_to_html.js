const showdown  = require('showdown');
const fs        = require('fs');

const converter = new showdown.Converter({ ghCompatibleHeaderId: true, tables: true });
const styles = `
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://unpkg.com/picnic">
<link rel="stylesheet" href="styles.css">
</head>
<body>
`

fs.readFile("public/lab-guide.md", "utf8", function(err, data) {
  if (err) throw err;
  const html = converter.makeHtml(data);
  const path = 'public/index.html'
  
  fs.writeFile(path, styles, (err) => {
    if (err) throw err;
  });

  fs.appendFile(path, html, (err) => {
    if (err) throw err;
  });

  fs.appendFile(path, "</body>", (err) => {
    if (err) throw err;
    console.log(`${path} has been written!`);
  });
});

